<?php

namespace Tests\Feature;

use App\Opinion;
use App\User;
use function create;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class opinionTest extends TestCase
{
    use RefreshDatabase;
    public function test_user_has_opinions()
    {
        $user=create(User::class);
        $opinion=create(Opinion::class,[
            "opinionText"=>'I didnt like it',
            "user_id"=>$user->id,
            "clientIP"=>"255.255.244.255",
        ]);
        $this->assertCount(1,$user->opinions->toArray());
        $this->assertEquals($user->name,$opinion->user->name);
    }

}
