<?php

namespace Tests\Feature;

use App\User;
use function create;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class homeTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_display_link_to_logged_in_user()
    {
        $user =  factory(User::class)->create();
        $this->actingAs($user);
        $this->get("/home")
        -> assertsee("/giveopinion/".$user->email);
    }
    public function test_dont_display_link_to_guest()
    {
        $this->withExceptionHandling();
        $this->get("/home")
            -> assertDontSee("/giveopinion/");
    }
    public function test_guest_can_give_opinion()
    {
        $user =  factory(User::class)->create();
        $this->actingAs($user);
        $url = '/giveopinion/' . $user->email;
//        dd($url);
        $this->get($url)
        ->assertSee($user->name)
        ->assertSee("your view");

    }

//    public function test_guest_gives_opinion_user_sees()
//    {
//        $user=create(User::class);
//        $url = '/giveopinion/' . $user->email;
//        $opinionText = "i liked it";
//        $this->post("/opinion",[
//            "opinionText"=> $opinionText,
//            "user_id"=>$user->id,
//        ]);
//        $this->actingAs($user);
//        $this->get("/home")
//            ->assertSee($opinionText);
//
//    }
}
