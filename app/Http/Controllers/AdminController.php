<?php

namespace App\Http\Controllers;

use App\Opinion;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function seeMatches()
    {
        $this->authorize('view', Opinion::class);
        $matches = DB::table('users')
                ->join('opinions', 'users.clientIP', '=', 'opinions.clientIP')
                ->select('users.*', 'opinions.opinionText')
                ->get();
            return view('matches')
                ->with('matches', $matches);

    }
    //
}
