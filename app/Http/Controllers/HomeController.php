<?php

namespace App\Http\Controllers;

#use http\Url;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Load;
use function redirect;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except("giveOpinion","pay","paid","view_orders");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function giveOpinion($userName)
    {
        $user=User::where("email",$userName)->first();

        return view('giveOpinion')
            ->with("user",$user);
    }
    public function index()
    {
        $user=auth()->user();
        $link=URL::to('/giveopinion/'.$user->email);
        //dd($user->opinions);
        return view('home')
            ->with("link",$link)
            ->with("opinions",$user->opinions);

    }
    
    public function pay(Request $request)
    {
        $delivery_location=request("formattedAddress_delivery");
        $collection_location=request("formattedAddress_collection");
//         dd($delivery_location,$collection_location);
        $newLoad=Load::create([
            "deliveryLocation"=>$delivery_location,
            "collectionLocation"=>$collection_location,
        ]);
        //dd($newLoad);
        return redirect("/paid");
        //        dd($newOpinion);
    }
    
    public function paid()
    {

        return view('paid');
        
    }
    
    public function view_orders()
    {
        $loads = Load::all();
        return view('myDeliveries')
        ->with("deliveries",$loads);
//         dd($deliverys);
    }
}
