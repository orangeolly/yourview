@extends('layouts.dbike_app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            	<div class="card-header">We can take your bike to your training destination</div>
                     <div class="card">
                        <div class="card-body">           	
            	<img src="http://img.clipartlook.com/cycling-fast-icon-bike-clip-art-350_225.jpg" width="50%" class="rounded mx-auto d-block">
            	</div>
            	</div>

                <form method="POST" action="/pay" id="deliveryForm">
                	@csrf
                    <div class="card">
                        <div class="card-body">
                            <div class="card-text">
                                <div class="row">
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="input-group">
                                            <input id="address_collection" type="text"
                                                   class="form-control cc-input"
                                                   placeholder="Type a collection location here and press ENTER"><br/>
                                            <span class="input-group-btn"></span>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <input readonly id="formattedAddress_collection"
                                               class="form-control cc-formatted-address" required
                                               name="formattedAddress_collection">
                                    </div>
                                </div>
                                
                                <div class="row">
                                <br><br>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="input-group">
                                            <input id="address_delivery" type="text"
                                                   class="form-control cc-input"
                                                   placeholder="Type a delivery location here and press ENTER"><br/>
                                            <span class="input-group-btn"></span>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <input readonly id="formattedAddress_delivery"
                                               class="form-control cc-formatted-address" required
                                               name="formattedAddress_delivery">
                                    </div>
                                </div>
                                <br>
                                <div class="price" class="row" style="display: none;">
                                
                                <div class="price" class="row"   style="display: none;">
                                    <div class="col text-center">
                                        <button type="button" id="pay_button"  onclick="send_form()" class="btn btn-primary"/>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </form>




            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/dbike.js') }}"></script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=*AIzaSyD0irdNMdYmEQGtn1JaKB7bfMO7NhBtC9Y&callback=apiready">

</script>

<!--script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDi5jBg7MgmeUCu76r84Hb99EwOsNPIfco&callback=apiready">

</script-->








@endsection


