@extends("layouts.app")

@section("content")
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">For user {{$user->name}}</div>

                <div class="card-body">
                    <form method="POST" action="/opinion">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="body">your view</label>
                            <textarea class="form-control" id="body" name="opinionText" value="edit required"></textarea>
                        </div>
                        <input type="hidden" name="user_id" value="{{$user->id}}">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Send</button>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
