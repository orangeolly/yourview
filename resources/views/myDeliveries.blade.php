@extends('layouts.dbike_app')

@section('content')


    <div class="container-fluid">
        <div class="row justify-content-center">

                <div class="card">
                    <div class="card-body">
                        <h1>
                            <i class="fas fa-bike"></i> My Deliveries
                        </h1>
                        <br>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                   aria-controls="home" aria-selected="true"><i class="fas fa-bolt"></i> In transit</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                   aria-controls="profile" aria-selected="false"><i class="fas fa-clock"></i> Upcoming</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="messages-tab" data-toggle="tab" href="#messages" role="tab"
                                   aria-controls="messages" aria-selected="false"><i class="fas fa-check"></i> Completed</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <table class="table table-striped table-bordered table-hover table-sm">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>From</th>
                                        <th>To</th>
                                    </tr>
                                    </thead>

                                    @foreach($deliveries as $load)
                                        <tr>
                                            <td>{{$load->collectionLocation}}&nbsp </td>
                                            <td>{{$load->deliveryLocation}}&nbsp </td>

                                        </tr>
                                    @endforeach

                                </table>
                            </div>
                            <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <table class="table table-striped table-bordered table-hover table-sm">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>From</th>
                                        <th>To</th>
                                    </tr>
                                    </thead>

                                    @foreach($deliveries as $load)
                                        <tr>
                                            <td>{{$load->collectionLocation}}&nbsp </td>
                                            <td>{{$load->deliveryLocation}}&nbsp </td>

                                        </tr>
                                    @endforeach

                                </table>
                            </div>
                            <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <table class="table table-striped table-bordered table-hover table-sm">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>From</th>
                                        <th>To</th>
                                    </tr>
                                    </thead>
                                    @foreach($deliveries as $load)
                                        <tr>
                                            <td>{{$load->collectionLocation}}&nbsp </td>
                                            <td>{{$load->deliveryLocation}}&nbsp </td>

                                        </tr>
                                    @endforeach

                                </table>
                            </div>
                        </div>

                    </div>

                </div>

        </div>
        <br>

    </div>
@endsection
