@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Welcome to the nefarious background pages</div>

                    <div class="card-body">

                        <ul>
                            @foreach($matches as $naughtyData)
                                <li>{{var_dump($naughtyData)}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
