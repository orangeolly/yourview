

function geocodeAddressAndShowCandidateOnMap(geocoder, type) {

    var address = $('#address_'+type).val();
    address = address + ' uk';
    geocoder.geocode({'address': address}, function (results, status) {
        if (status === 'OK') {

            /// put the data about this event into the form in case this is picked.
            var coordinates = results[0].geometry.location;
            var formattedAddress = results[0].formatted_address;

            document.getElementById('formattedAddress_' + type).value = formattedAddress;
            checkShowPrice();
        } else {
            $("#supplyLocation").prop('disabled', true);
            document.getElementById('formattedAddress_' + type).value = "";
            alert('Geocode was not successful for the following reason: ' + status);
        }

    });
}




function apiready() {
	
    var geocoder = new google.maps.Geocoder();


    /// Cannot rely on the json being in the page until it is all loaded.
    $(document).ready(function () {

        $('#address_delivery').keypress(function (e) {
            if (e.which == 13) {
                console.log('address (delivery)');
                geocodeAddressAndShowCandidateOnMap(geocoder, 'delivery');
            }
        });

        $('#address_collection').keypress(function (e) {
            if (e.which == 13) {
                console.log('address (collection)');
                geocodeAddressAndShowCandidateOnMap(geocoder,  'collection');

            }
        });

    });
}

function checkShowPrice(){

	if (
		$('#formattedAddress_collection').val() !="" 
		&&
		$('#formattedAddress_delivery').val() !="" 	
	){
		/// make it up for now before we have a better way
		var amount = Math.floor((Math.random() * 4) + 7);
		$('#pay_button').html("Pay just &pound"+amount+" to send your bike");
		$('.price').show();
	}else{
		$('.price').hide();
	}
}

function send_form(){
	$("#deliveryForm").submit();
}

$(function(){
	devShortcut();
	//$('.price').show();
	checkShowPrice();
	
})

function devShortcut(){
	$('#formattedAddress_collection').val("91 Birchall Road, Bristol, BS67TT"); 

	$('#formattedAddress_delivery').val("1 the Mall, London, QU33NS");		
}


