<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
//    dd(" in the root / route");
    return view('bike_home');
//     return view('test');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/pay', 'HomeController@pay')->name('pay');
Route::get('/paid', 'HomeController@paid')->name('paid');
Route::get('/view_orders', 'HomeController@view_orders')->name('view_orders'); 
Route::get('/giveopinion/{username}', 'HomeController@giveOpinion');
Route::resource('opinion','OpinionController');
Route::get('/admin/seeMatches', 'AdminController@seeMatches');



//Route::get('/giveopinion/{username}', function($username){
//    dd($username);
//});
